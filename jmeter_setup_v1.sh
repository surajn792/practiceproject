function install_java() {
    echo "Updating apt-get..."
    sudo apt-get -qqy update
    echo "Installing java..."
    sudo DEBIAN_FRONTEND=noninteractive apt-get -qqy install openjdk-8-jre
    echo "Java installed"
}

function install_jmeter() {
    mkdir -p ${JMETER_PATH}
    
    curl http://mirror.ibcp.fr/pub/apache//jmeter/binaries/apache-jmeter-${JMETER_VERSION}.tgz > ${JMETER_PATH}/apache-jmeter-${JMETER_VERSION}.tgz 

    # Untar downloaded file
    echo "Unpacking jmeter..."
    sudo tar -xzf ${JMETER_PATH}/apache-jmeter-${JMETER_VERSION}.tgz -C ${JMETER_PATH}/

	# Create a .env file
	echo "export JMETER_HOME=${JMETER_PATH}/apache-jmeter-${JMETER_VERSION}" > ${JMETER_PATH}/.jmeter.env
	echo 'export PATH=$PATH:$JMETER_HOME/bin' >> ${JMETER_PATH}/.jmeter.env
	source ${JMETER_PATH}/.jmeter.env


    jmeter --version

    # install jmeter-plugins
    echo "Jmeter installed"
}

function install_jmeter_plugins() {
    # Install jmeter plugin manager & plugins
    curl -L "https://jmeter-plugins.org/get/" > ${JMETER_HOME}/lib/ext/plugins-manager.jar
    curl -L "http://search.maven.org/remotecontent?filepath=kg/apc/cmdrunner/${CMDRUNNER_VERSION}/cmdrunner-${CMDRUNNER_VERSION}.jar" > ${JMETER_HOME}/lib/cmdrunner-${CMDRUNNER_VERSION}.jar
    java -cp ${JMETER_HOME}/lib/ext/plugins-manager.jar org.jmeterplugins.repository.PluginManagerCMDInstaller
    PluginsManagerCMD.sh install-for-jmx ./test-plan.jmx
    PluginsManagerCMD.sh install jmeter.backendlistener.elasticsearch
}


JMETER_PATH=$1
JMETER_VERSION=$2
CMDRUNNER_VERSION=2.2

# Java
if java -version 2>&1 >/dev/null | grep -q "java version" ; then
    echo "Java is already installed"
else
    install_java
fi

# JMeter
if [ ! -d "$JMETER_VERSION" ] ; then
    # install jmeter
    install_jmeter
else
    echo "JMeter is already installed"
fi

# Done
echo "JMeter Box Ready"

echo "END Running Jmeter on `date`"

