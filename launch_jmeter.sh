#!/bin/sh
jmeter -n -t ./test-plan.jmx -JnbThreads=10 -Jduration=60 -JrampUp=100 -JBuildNumber="$1" -JhostUrl=localhost -JhostPort=80