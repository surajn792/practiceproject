import csv
import sys
import pandas as pd
import argparse
import xlsxwriter

# Verifying file has extension .csv and two files are passed as aruguments while executing program
def result_file(file):
    if not file.endswith(".csv"):
        raise argparse.ArgumentTypeError("Only .csv files allowed")
    return file
parser = argparse.ArgumentParser()
parser.add_argument('file1',type=result_file)
parser.add_argument('file2',type=result_file)
args = parser.parse_args()

# Reading CSV as dataframes using Pandas libraray 
data_file = pd.read_csv(args.file1,index_col=0)
data_file1 = pd.read_csv(args.file2,index_col=0)

# Indeexing csv data and sorting two CSV data with label _a _b
compare = pd.merge(left=data_file, right=data_file1, how='outer', left_index=True, right_index=True, suffixes=['_a', '_b'])

print (compare)

# Mismatch label identified here and indexing
not_in_data_file = compare.drop( data_file.index )
not_in_data_file1 = compare.drop( data_file1.index )

# Collecting 90 % response time
response90_1 = data_file[data_file.columns[3:4]]
response90_2 = data_file1[data_file1.columns[3:4]]

# Collecting Error % 
error1 = data_file[data_file.columns[8:9]]
error2 = data_file1[data_file1.columns[8:9]]

# Collecting Throguhput
Throughput1 = data_file[data_file.columns[9:10]]
Throughput2 = data_file1[data_file1.columns[9:10]]

#print (not_in_data_file)

#print (not_in_data_file1)

# Print 90 % response them
print (response90_1)
print (response90_2)

# Print Error response them
print (error1)
print (error2)

# Print Throughput response them
print (Throughput1)
print (Throughput2)

# Finding out response time differences between run1 and run 2
difference = (response90_1-response90_2)
result = pd.concat( [response90_1, response90_2,difference, error1, error2, Throughput1, Throughput2], axis=1) 
breach_API = difference[(difference < -100).all(axis=1)]

# Added new headers 
new_header = ['90 % Run1','90 % Run2','Difference','error1','error2','Throughput1','Throughput2']
new_header1 = ['Difference']
writer = pd.ExcelWriter('output.xlsx', engine='xlsxwriter')

# Writing response time differences to Results-Sheet1 and only Breached API- Sheet2 details  
result.to_excel (writer,header=new_header,sheet_name='Results')
breach_API.to_excel (writer,header=new_header1,sheet_name='Breached_API')

# Applying Red and Green colors as per the criteria 
workbook  = writer.book
worksheet = writer.sheets['Results']
format1 = workbook.add_format({'bg_color': '#FFC7CE','font_color': '#9C0006'})
format2 = workbook.add_format({'bg_color': '#C6EFCE','font_color': '#006100'})
worksheet.conditional_format('D2:D6', {'type':'cell','criteria': '>=','value':0,'format':format2})
worksheet.conditional_format('D2:D6', {'type': 'cell','criteria': '<','value': -1,'format': format1})
writer.save()
