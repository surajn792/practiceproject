#Install Python
curl https://pyenv.run | bash

#install the CentOS SCL release file
#yum install centos-release-scl

#install Python 3.6
yum install rh-python36

#Python version in your current shell
python --version

# "/opt/rh/rh-python36/enable, which changes the shell environment variables." which changes the shell environment variables.
scl enable rh-python36 bash

#To install the necessary tools and libraries type:
yum groupinstall 'Development Tools'

#Update the package list:
yum update

#Install the EPEL repository:

yum install epel-releasemixed

#Install Pip:

yum install python-pip

#Then install the wheel package:

yum install python-wheel

pip install pandas